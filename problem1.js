function getCarInfoById(inventory, id) {
  let totalCars = inventory.length;
  if (!totalCars) {
    console.log("Inventory is empty");
    return -1;
  } else if (id < 1 || id > totalCars - 1) {
    console.log("Invalid ID entered");
    return -1;
  }
  for (let index = 0; index < totalCars; index++) {
    if (inventory[index].id === id) {
      return inventory[index];
    }
  }
  return null;
}

module.exports = getCarInfoById;
