let getAllCarYears = require("./problem4.js");

function getOldCars(inventory) {
  if (inventory.length < 1) {
    console.log("Inventory is empty");
    return null;
  } else {
    let allCarYears = getAllCarYears(inventory);
    let oldCars = [];
    for (let index = 0; index < allCarYears.length; index++) {
      if (allCarYears[index] > 2000) {
        oldCars.push(allCarYears[index]);
      }
    }
    return oldCars;
  }
}

module.exports = getOldCars;
