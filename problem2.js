function getLastCarInfo(inventory) {
  if (inventory.length > 0) {
    let lastCarIndex = inventory.length - 1;
    return inventory[lastCarIndex];
  } else {
    return null;
  }
}

module.exports = getLastCarInfo;
