function getAllCarYears(inventory) {
  if (inventory.length < 1) {
    console.log("Inventory is empty");
    return null;
  } else {
    let allCarYears = [];
    for (let index = 0; index < inventory.length; index++) {
      allCarYears.push(inventory[index].car_year);
    }

    return allCarYears;
  }
}

module.exports = getAllCarYears;
