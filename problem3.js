function sortCarsAlphabetically(inventory) {
  if (inventory.length < 1) {
    console.log("Inventory is empty");
    return null;
  } else {
    let carModels = [];
    for (let index = 0; index < inventory.length; index++) {
      carModels.push(inventory[index].car_model);
    }
    carModels.sort();
    return carModels;
  }
}

module.exports = sortCarsAlphabetically;
