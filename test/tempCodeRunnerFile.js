const inventory = require('../data');
let getCarInfoById = require('../problem1');

let car = getCarInfoById(inventory,-1);

if (car) {
    console.log(`Car 33 is a ${car.car_year} ${car.car_make} ${car.car_model}`);
} else {
    console.log(`Car 33 not found in the inventory.`);
}