const inventory = require("../data");
let getCarInfoById = require("../problem1");

let car = getCarInfoById(inventory, 33);

if (car != -1) {
  if (car) {
    console.log(
      `Car ${car.id} is a ${car.car_year} ${car.car_make} ${car.car_model}`
    );
  } else {
    console.log(`Car ${car.id} not found in the inventory.`);
  }
}
