const inventory = require("../data");
let getLastCarInfo = require("../problem2");

let lastCarInfo = getLastCarInfo(inventory);
if (lastCarInfo) {
  console.log(`Last car is a ${lastCarInfo.car_make} ${lastCarInfo.car_model}`);
} else {
  console.log(`Inventory is empty`);
}
