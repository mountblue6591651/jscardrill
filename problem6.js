// Function to filter BMW and Audi cars from the inventory
function getBMWAndAudi(inventory) {
  if (inventory.length < 1) {
    console.log("Inventory is empty");
    return null;
  } else {
    let BMWAndAudi = [];
    for (let index = 0; index < inventory.length; index++) {
      if (
        inventory[index].car_make === "Audi" ||
        inventory[index].car_make === "BMW"
      ) {
        BMWAndAudi.push(inventory[index]);
      }
    }
    if (BMWAndAudi.length === 0) {
      console.log("No BMW and Audi cars available");
      return null;
    } else {
      return BMWAndAudi;
    }
  }
}

module.exports = getBMWAndAudi;
